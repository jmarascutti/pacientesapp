<?php

Route::get('/', function () {
    return view('index');
});

Route::get('pacientes/buscarPorEstado', 'PacientesController@buscarPorEstado')->name('pacientes.buscarPorEstado');

Route::get('pacientes/buscarPorEstadoTodos', 'PacientesController@buscarPorEstadoTodos')->name('pacientes.buscarPorEstadoTodos');

Route::get('pacientes/buscarPorCriterio', 'PacientesController@buscarPorCriterio')->name('pacientes.buscarPorCriterio');

Route::get('pacientes/buscarPorCriterioTodos', 'PacientesController@buscarPorCriterioTodos')->name('pacientes.buscarPorCriterioTodos');

Route::get('pacientes/alta', function () {
    return view('new');
});

Route::post('pacientes/alta', 'PacientesController@crear')->name('pacientes.crear');

Route::get('pacientes/ver/{id}', 'PacientesController@ver')->name('pacientes.ver');

Route::get('pacientes/editar/{id}', 'PacientesController@editar')->name('pacientes.editar');

Route::put('pacientes/editar/{id}', 'PacientesController@update')->name('pacientes.update');
    
Route::get('pacientes/resguardarInformacion', 'ResguardarController@resguardar')->name('pacientes.resguardarInformacion');

Route::get('pacientes/restaurar', function () {
    return view('restaurar');
});

Route::post('pacientes/restaurar', 'RestaurarController@restaurar')->name('pacientes.restaurarInformacion');