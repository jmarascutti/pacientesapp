@extends('plantilla')
<script language="JavaScript">
    function validarExtensionVacio() {
        var file = document.querySelector("#inputFileRestauracion");
        // validar que el input file no esté vacío
        if(document.formRestauracion.inputFileRestauracion.value==""){
            alert("Por favor, haga clic en 'Seleccionar archivo' y elija uno.");
            return false;
        } else {
            // validar que la extensión del archivo elegido sea .SQL
            if (/\.(sql)$/i.test(file.files[0].name) === false) {
                alert("El archivo seleccionado debe tener extensión .sql");
                $(file).val('');
                return false;
            }
        }   
    }  
</script>

  <title>Restauración de Pacientes | Sistema de Gestión de Pacientes</title>

@section('seccion')
    
  <div class="p-3 mb-5 pb-5 bg-light"> 
    <div class="display-4 pl-4 pt-4 pr-4 text-center font-weight-normal">Restauración de Pacientes</div>      
        <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4 pb-3">
        <h4 class="mr-5 text-center font-weight-normal">Elija un archivo..</h4>             
        <form class=" mt-4 mb-3" enctype="multipart/form-data" action="{{ route('pacientes.restaurarInformacion') }}" name="formRestauracion" method="POST">
            @csrf 
            <div class="row justify-content-md-center">            
                <div class="col col-lg-5">     
                    <div class="card bg-light mb-3" style="">
                        <div class="card-body">
                            <input name="inputFileRestauracion" id="inputFileRestauracion" type="file" class=" w-100" aria-describedby="emailHelp">
                        </div>
                    </div>                           
                </div>          
                <div class="col col-lg-3 mt-3">
                    <input type="submit" class="btn btn-lg btn-success pt-1 pb-1 pr-3 pl-3 rounded" onClick="return validarExtensionVacio(this.form)" value="Restaurar">
                    <button type="button" class="btn btn-lg btn-success ml-3 pt-1 pb-1 pr-3 pl-3 rounded" onclick="window.history.back()">Volver</button>  
                </div>
            </div>  
        </form>
    </div>
  </div>

@endsection