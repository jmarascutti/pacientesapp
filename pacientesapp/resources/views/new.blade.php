@extends('plantilla')

  <title>Alta de Pacientes | Sistema de Gestión de Pacientes</title>

@section('seccion')
      
  <div class="display-4 pl-4 pt-4 pr-4 text-center font-weight-normal">Alta de Paciente</div>

  @if (session('mensaje'))
    <div class="alert alert-success alert-dismissible container mt-2" role="alert">
      <strong>{{ session('mensaje') }}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <!-- Se verifica si existen errores de validación. En caso que existan, los pinta en pantalla -->
  @if (count($errors) > 0)             
    <div class="alert alert-warning alert-dismissible container mt-4" role="alert">           
      <strong>¡ATENCION!</strong>     
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button> 
      <ul>
        @foreach ($errors->all() as $error)
          <li class="mt-1">{{ $error }}</li>
        @endforeach
      </ul>               
    </div>
  @endif        

  <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4">
    <form class="p-4" action="{{ route('pacientes.crear') }}" method="POST">
      @csrf                                                  
          
      <div class="form-group">
        <label for="labelApellido">Apellido</label>
        <input type="text" class="form-control form-control" id="inputApellido" aria-describedby="emailHelp" placeholder="Ingrese aquí el apellido del paciente" name="apellido" value="{{ old('apellido')}}" maxlength="50">
      </div>             
      <div class="form-group">
        <label for="labelNombre">Nombre</label>
        <input type="text" class="form-control form-control" id="inputNombre" aria-describedby="emailHelp" placeholder="Ingrese aquí el nombre del paciente" name="nombre" value="{{ old('nombre')}}" maxlength="50">
      </div>             
      <div class="form-group">
        <label for="labelEdad">Edad</label>
        <input type="text" class="form-control form-control" id="inputEdad" aria-describedby="emailHelp" placeholder="Ingrese aquí la edad del paciente" name="edad" value="{{ old('edad')}}" maxlength="3">
      </div>             
      <div class="form-group">
        <label for="labelDNI">DNI</label>
        <input type="text" class="form-control form-control" id="inputDNI" aria-describedby="emailHelp" placeholder="Ingrese aquí el DNI del paciente" name="dni" maxlength="12">
      </div>
      <div class="form-group">
        <label for="labelFechaNac">Fecha de nacimiento</label>
        <input type="date" class="form-control form-control" id="inputFechaNac" aria-describedby="emailHelp" placeholder="Ingrese aquí la fecha de nacimiento del paciente" name="fec_nac">
      </div>
      <div class="form-group">
        <label for="labelDireccion">Dirección</label>
        <input type="text" class="form-control form-control" id="inputDireccion" aria-describedby="emailHelp" placeholder="Ingrese aquí la dirección del paciente" name="direccion" maxlength="50">
      </div>
      <div class="form-group">
        <label for="labelCelular">Celular</label>
        <input type="text" class="form-control form-control" id="inputCelular" aria-describedby="emailHelp" placeholder="Ingrese aquí el celular del paciente" name="cel" maxlength="20">
      </div>
      <div class="form-group">
        <label for="labelTelefono">Teléfono</label>
        <input type="text" class="form-control form-control" id="inputTelefono" aria-describedby="emailHelp" placeholder="Ingrese aquí el teléfono fijo del paciente" name="tel" maxlength="20">
      </div>
      <div class="form-group">
        <label for="labelEmail">Email</label>
        <input type="text" class="form-control form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Ingrese aquí el email del paciente" name="mail" maxlength="50">
      </div>
      <div class="form-group">
        <label for="labelObraSocial">Nombre de la obra social</label>
        <input type="text" class="form-control form-control" id="inputObraSocial" aria-describedby="emailHelp" placeholder="Ingrese aquí el nombre de la obra social que posee el paciente" name="ob_soc" maxlength="50">
      </div>
      <div class="form-group">
        <label for="labelNroObraSocial">Número de obra social</label>
        <input type="text" class="form-control form-control" id="inputNroObraSocial" aria-describedby="emailHelp" placeholder="Ingrese aquí el número de la obra social que posee el paciente" name="num_ob_soc" maxlength="20">
      </div>
      <div class="form-group">
        <label for="labelEstado">Estado</label>
        <input type="text" class="form-control form-control" id="inputEstado" aria-describedby="emailHelp" value="Por llamar" name="estado" readonly>
      </div>
      <div class="form-group">
        <label for="labelObsevaciones">Observaciones</label>
        <textarea class="form-control" name="observaciones" id="" cols="30" rows="4" maxlength="250"></textarea>
      </div>
      
      <div class="form-group text-center mt-4">
        <button type="submit" class="btn btn-lg btn-success pl-2 pr-2 ml-2 mr-2 rounded text-center">Guardar</button>
        <button type="button" class="btn btn-lg btn-success pl-3 pr-3 ml-2 mr-2 rounded text-center" onclick="window.history.back()">Volver</button>
      </div>
    </form>
  </div>

@endsection