@extends('plantilla')

  <title>Ver Paciente | Sistema de Gestión de Pacientes</title>

@section('seccion')

  <div class="p-3 mb-4  bg-light"> 
    <div class="display-4 pl-4 pt-4 pr-4 text-center font-weight-normal">Ver Paciente</div>
    <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4">
      <form class="p-4">
        <div class="form-group">
          <label for="labelApellido">Apellido</label>
          <input type="text" class="form-control form-control" id="inputApellido" aria-describedby="emailHelp" value="{{ $paciente->apellido }}" readonly>
        </div>
        <div class="form-group">
          <label for="labelNombre">Nombre</label>
          <input type="text" class="form-control form-control" id="inputNombre" aria-describedby="emailHelp" value="{{ $paciente->nombre }}" readonly>
        </div>
        <div class="form-group">
          <label for="labelEdad">Edad</label>
          <input type="text" class="form-control form-control" id="inputEdad" aria-describedby="emailHelp" value="{{ $paciente->edad }}" maxlength="3" readonly>
        </div>
        <div class="form-group">
          <label for="labelDNI">DNI</label>
          <input type="text" class="form-control form-control" id="inputDNI" aria-describedby="emailHelp" value="{{ $paciente->dni }}" maxlength="12" readonly>
        </div>
        <div class="form-group">
          <label for="labelFechaNac">Fecha de nacimiento</label>
          <input type="text" class="form-control form-control" id="inputFechaNac" aria-describedby="emailHelp" value="{{ $paciente->fec_nac }}" readonly>
        </div>
        <div class="form-group">
          <label for="labelFechaNac">Dirección</label>
          <input type="text" class="form-control form-control" id="inputDireccion" aria-describedby="emailHelp" value="{{ $paciente->direccion }}" readonly>
        </div>
          <div class="form-group">
            <label for="labelCelular">Celular</label>
            <input type="text" class="form-control form-control" id="inputCelular" aria-describedby="emailHelp" value="{{ $paciente->cel }}" readonly>
          </div>
          <div class="form-group">
            <label for="labelTelefono">Teléfono</label>
            <input type="text" class="form-control form-control" id="inputTelefono" aria-describedby="emailHelp" value="{{ $paciente->tel }}" readonly>
          </div>
          <div class="form-group">
            <label for="labelEmail">Email</label>
            <input type="text" class="form-control form-control" id="inputEmail" aria-describedby="emailHelp" value="{{ $paciente->mail }}" readonly>
          </div>
          <div class="form-group">
            <label for="labelObraSocial">Nombre de la obra social</label>
            <input type="text" class="form-control form-control" id="inputObraSocial" aria-describedby="emailHelp" value="{{ $paciente->ob_soc }}" readonly>
          </div>
          <div class="form-group">
            <label for="labelNroObraSocial">Número de obra social</label>
            <input type="text" class="form-control form-control" id="inputNroObraSocial" aria-describedby="emailHelp" value="{{ $paciente->num_ob_soc }}" readonly>
          </div>
          <div class="form-group">
            <label for="labelEstado">Estado</label>
            <input type="text" class="form-control form-control" id="inputEstado" aria-describedby="emailHelp" value="{{ $paciente->estado}}" readonly>
          </div>
          <div class="form-group">
            <label for="labelEstado">Observaciones</label>
            <textarea class="form-control" name="" id="" cols="30" rows="4" readonly>{{ $paciente->observaciones }}</textarea>
          </div>

          <div class="form-group text-center mt-4">
            <a class="btn btn-lg btn-success pl-2 pr-2 ml-2 mr-2 rounded text-center" href="/pacientes/editar/{{ $paciente->id }}" class="btn btn-xs btn-info pull-right">Modificar</a>               
            <button type="button" class="btn btn-lg btn-success pl-3 pr-3 ml-2 mr-2 rounded text-center" onclick="window.history.back()">Volver</button>
          </div>
      </form>
    </div>
  </div>

@endsection


    
  