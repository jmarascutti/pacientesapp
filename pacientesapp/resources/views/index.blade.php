<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('img/SGPmini.png')}}" >
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}" >
    <title>Sistema de Gestión de Pacientes</title>
  </head>

  <body>
    <div class="container navbar-dark bg-success mt-4 p-2 shadow-lg">
      <div class="text-center m-5">
        <img class="img-fluid" src="{{ asset('img/SGP.png') }}" alt="Sistema de Gestión de Pacientes" width="800" height="200">
      </div>
      <br>
      <div class="text-center row m-4 p-4 border">
        <div class="col-sm-4">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link font-weight-normal text-uppercase" href="{{ url('/pacientes/alta') }}">Alta</a>
            </li>
          </ul>
        </div>
        <div class="col-sm-4">
          <ul class="navbar-nav">
            <li class="nav-item">              
              <a class="nav-link font-weight-normal text-uppercase" href="{{ url('/pacientes/buscarPorCriterioTodos') }}">Buscar por criterio</a>
            </li>
          </ul>
        </div>
        <div class="col-sm-4">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link font-weight-normal text-uppercase" href="{{ url('/pacientes/buscarPorEstadoTodos') }}">Buscar por estado</a>                
            </li>            
          </ul>
        </div>
      </div>
    </div>

    <footer class="row text-light align-items-center mt-5">
      <div class="col-sm-12">
        <p class="text-center mb-0 pl-3 text-dark">2020 - Copyright ©</p>
      </div>
      <div class="text-center col-sm-12">
      <a class="" href="http://www.headesign.com.ar"><img class="img-fluid" src="{{URL::asset('img/logo.png')}}" alt="logo de la empresa" width="200" height="50"></a>      
      </div>
    </footer>
  </body>
</html>