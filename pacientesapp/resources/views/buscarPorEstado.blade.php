@extends('plantilla')

  <title>Búsqueda de Pacientes por Estado | Sistema de Gestión de Pacientes</title>

@section('seccion')
    
  <div class="p-3 mb-4  bg-light"> 
    <div class="display-4 pl-4 pt-4 pr-4 text-center font-weight-normal">Buscar por un estado</div>
    <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4 pb-4">
      <h4 class="mr-5 text-center font-weight-normal">Elige un estado</h4>
      <form class="mt-4 text-center" action="{{ route('pacientes.buscarPorEstado') }}"  method="GET" >
        <div class="form-group mb-2 ">   
          <select class="custom-select w-25"  id="selectOptionEstado" name="Estado" >
            <option value="Por llamar">Por llamar</option>
            <option value="Sin atender">Sin atender</option>
            <option value="Llamado">Llamado</option>                    
          </select>
          <button type="submit" class="btn btn-lg btn-success ml-4 pt-1 pb-1 pr-3 pl-3 rounded">Buscar</button>
          <button type="button" class="btn btn-lg btn-success ml-3 pt-1 pb-1 pr-3 pl-3 rounded" onclick="window.history.back()">Volver</button>     
        </div>           
      </form>                 
    </div>

    <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4 pb-4">
      <h4 class="font-weight-normal">Resultados de la búsqueda</h4>

      @if (isset($estado))
        <div class="alert alert-primary alert-dismissible container mt-4" role="alert">
          <strong>Se buscó por estado: {{$estado}}  </strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      <table class="table table-hover table-bordered mt-4">
        <thead class="thead-dark">
          <tr>
            <th scope="col" class="text-center">Apellido</th>
            <th scope="col" class="text-center">Nombre</th>
            <th scope="col" class="text-center">Estado</th>
            <th scope="col" class="text-center">Acciones</th>
          </tr>
        </thead>
        <tbody>  
          @if (count($pacientes) == 0)
            <td colspan="4" class="text-center align-middle font-weight-bold">No existen pacientes. </td>
          @endif
          @foreach ($pacientes as $paciente)                       
            <tr>
              <td class="text-center align-middle">{{ $paciente->apellido }}</td>
              <td class="text-center align-middle">{{ $paciente->nombre }}</td>
              <td class="text-center align-middle">{{ $paciente->estado }}</td>
              <td class="text-center align-middle">
                <a class="btn btn-success pl-3 pr-3 rounded" href="/pacientes/ver/{{ $paciente->id }}">Ver</a>
                <a class="btn btn-success pl-3 pr-3 rounded" href="/pacientes/editar/{{ $paciente->id }}">Modificar</a>               
              </td>
            </tr>            
          @endforeach                                                                    
        </tbody>  
         
        <div class="text-center mt-4">
          <!-- Al cambiar de página, se renderiza y no se pierden los filtros aplicados (estado) -->
          {!! $pacientes ->appends(Request::only(['Estado','type']))-> render() !!}  
        </div>         
      </table>   
    </div>        
  </div>

@endsection