<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('img/SGPmini.png')}}" >
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}">
    <script type='text/javascript' src="{{ asset('js/jquery-2.0.2.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>-->        
  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-success p-4 shadow-lg">
      <a class="navbar-brand" href="{{ url('/') }}"><img class="img-fluid" src="{{ asset('img/SGP.png') }}" alt="Sistema de Gestión de Pacientes" width="300" height="75"></a>
      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto ml-4">
          <li class="nav-item ml-2">
            <a class="nav-link" href="{{ url('/pacientes/alta') }}">Alta</a>
          </li>
          <li class="nav-item ml-2">              
            <a class="nav-link" href="{{ url('/pacientes/buscarPorCriterioTodos') }}">Buscar por criterio</a>
          </li>
          <li class="nav-item ml-2 mr-5 pr-5">
            <a class="nav-link" href="{{ url('/pacientes/buscarPorEstadoTodos') }}">Buscar por estado</a>                
          </li> 
          <li class="nav-item ml-5 pl-5">
            <a class="nav-link" href="{{ url('/pacientes/resguardarInformacion') }}">Resguardar información</a>     
          </li>   
          <li class="nav-item ml-2">
            <a class="nav-link" href="{{ url('/pacientes/restaurar') }}">Restaurar información</a>                
          </li>          
        </ul>
      </div>
    </nav>

    <div class="p-3 mb-4  bg-light"> 
      @yield('seccion') 
    </div>

    <footer class="bg-success p-4 row text-light align-items-center">
      <div class="col-sm-12">
        <p class="text-right m-0 pr-4">2020 - Copyright ©</p>
      </div>
      <div class="text-right col-sm-12">
           <a class="" href="http://www.headesign.com.ar"><img class="img-fluid" src="{{URL::asset('img/logo.png')}}" alt="logo de la empresa" width="200" height="50"></a>      
      </div>
    </footer>
  </body>
</html>