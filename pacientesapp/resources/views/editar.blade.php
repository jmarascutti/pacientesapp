@extends('plantilla')

  <title>Modificación de Pacientes | Sistema de Gestión de Pacientes</title>

@section('seccion')
  
  <div class="display-4 pl-4 pt-4 pr-4 text-center font-weight-normal">Modificación de Paciente</div>

    @if (session('mensaje'))
      <div class="alert alert-success alert-dismissible container mt-4" role="alert">
        <strong>{{ session('mensaje') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    <!-- Se verifica si existen errores de validación. En caso que existan, los pinta en pantalla -->
    @if (count($errors) > 0)             
      <div class="alert alert-warning alert-dismissible container mt-4" role="alert">           
          <strong>¡ATENCION!</strong>     
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> 
          <ul>
            @foreach ($errors->all() as $error)
              <li class="mt-1">{{ $error }}</li>
            @endforeach
          </ul>               
      </div>
    @endif

    <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4">
      <form class="p-4" action="{{ route('pacientes.update', $paciente->id) }}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group">
          <label for="labelApellido">Apellido</label>
          <input type="text" class="form-control form-control" id="inputApellido" aria-describedby="emailHelp" name="apellido"  value="{{ $paciente->apellido }}" maxlength="50">
        </div>
        <div class="form-group">
          <label for="labelNombre">Nombre</label>
          <input type="text" class="form-control form-control" id="inputNombre" aria-describedby="emailHelp" name="nombre" value="{{ $paciente->nombre }}" maxlength="50">
        </div>
        <div class="form-group">
          <label for="labelEdad">Edad</label>
          <input type="text" class="form-control form-control" id="inputEdad" aria-describedby="emailHelp" name="edad" value="{{ $paciente->edad }}" maxlength="3">
        </div>
        <div class="form-group">
          <label for="labelDNI">DNI</label>
          <input type="text" class="form-control form-control" id="inputDNI" aria-describedby="emailHelp"  name="dni" value="{{ $paciente->dni }}" maxlength="12">
        </div>
        <div class="form-group">
          <label for="labelFechaNac">Fecha de nacimiento</label>
          <input type="date" class="form-control form-control" id="inputFechaNac" aria-describedby="emailHelp" name="fec_nac" value="{{ $paciente->fec_nac }}">
        </div>
        <div class="form-group">
          <label for="labelDireccion">Dirección</label>
          <input type="text" class="form-control form-control" id="inputDireccion" aria-describedby="emailHelp"  name="direccion" value="{{ $paciente->direccion }}" maxlength="50">
        </div>
        <div class="form-group">
          <label for="labelCelular">Celular</label>
          <input type="text" class="form-control form-control" id="inputCelular" aria-describedby="emailHelp" name="cel" value="{{ $paciente->cel }}" maxlength="20">
        </div>
        <div class="form-group">
          <label for="labelTelefono">Teléfono</label>
          <input type="text" class="form-control form-control" id="inputTelefono" aria-describedby="emailHelp"  name="tel"  value="{{ $paciente->tel }}" maxlength="20">
        </div>
        <div class="form-group">
          <label for="labelEmail">Email</label>
          <input type="text" class="form-control form-control" id="inputEmail" aria-describedby="emailHelp" name="mail" name="mail" value="{{ $paciente->mail }}" maxlength="50">
        </div>
        <div class="form-group">
          <label for="labelObraSocial">Nombre de la obra social</label>
          <input type="text" class="form-control form-control" id="inputObraSocial" aria-describedby="emailHelp" name="ob_soc" value="{{ $paciente->ob_soc }}" maxlength="50">
        </div>
        <div class="form-group">
          <label for="labelNroObraSocial">Número de obra social</label>
          <input type="text" class="form-control form-control" id="inputNroObraSocial" aria-describedby="emailHelp" name="num_ob_soc" value="{{ $paciente->num_ob_soc }}" maxlength="20">
        </div>
        <div class="form-group">
          <label for="labelEstado">Estado</label>
          @if ($paciente->estado == 'Por llamar')
            <select class="custom-select custom-select" name="estado">
              <option selected value="Por llamar">Por llamar</option>
              <option value="Sin atender">Sin atender</option>
              <option value="Llamado">Llamado</option>
            </select>
          @elseif ($paciente->estado == 'Sin atender')
            <select class="custom-select custom-select" name="estado">
              <option value="Por llamar">Por llamar</option>
              <option selected value="Sin atender">Sin atender</option>
              <option value="Llamado">Llamado</option>
            </select>
          @elseif ($paciente->estado == 'Llamado')
            <select class="custom-select custom-select" name="estado">
              <option value="Por llamar">Por llamar</option>
              <option value="Sin atender">Sin atender</option>
              <option selected value="Llamado">Llamado</option>
            </select>
          @endif
        </div>
        <div class="form-group">
          <label for="labelObservaciones">Observaciones</label>
          <textarea class="form-control" name="observaciones" id="observaciones" cols="30" rows="4" maxlength="250">{{ $paciente->observaciones }}</textarea>
        </div>
        
        <div class="form-group text-center mt-4">
          <button type="submit" class="btn btn-lg btn-success pl-2 pr-2 ml-2 mr-2 rounded text-center">Guardar</button>
          <button type="button" class="btn btn-lg btn-success pl-3 pr-3 ml-2 mr-2 rounded text-center" onclick="window.history.back()">Volver</button>
        </div>
      </form>
    </div>
  </div>

@endsection