@extends('plantilla')

  <title>Búsqueda de Pacientes por Criterio | Sistema de Gestión de Pacientes</title>

@section('seccion')

  <script>
    function ShowSelected() {       
      var combo = document.getElementById("selectOptionCriterio");
      var selected = combo.options[combo.selectedIndex].value;  
      if (selected == "Buscar por apellido") {
        document.getElementById('inputTextBusqueda').placeholder = 'Ingrese el apellido del paciente';
      } else {
        document.getElementById('inputTextBusqueda').placeholder = 'Ingrese el nombre del paciente';
      }
    }
  </script>
    
  <div class="p-3 mb-4  bg-light"> 
  <div class="display-4 pl-4 pt-4 pr-4 text-center font-weight-normal">Buscar por un criterio</div>      
    <div class="container shadow-lg bg-white mt-4 pl-4 pr-4 pt-4 pb-3">
      <h4 class="mr-5 text-center font-weight-normal">Elige un criterio</h4>             
      <form class=" mt-4 mb-3" action="{{ route('pacientes.buscarPorCriterio') }}"  method="GET">
        <div class="row justify-content-md-center">
          <div class="col col-lg-3">
            <select class="custom-select" id="selectOptionCriterio" onchange="ShowSelected();" name="Criterio">
              <option value="Buscar por apellido">Buscar por apellido</option>
              <option value="Buscar por nombre">Buscar por nombre</option>
            </select>  
          </div>
          <div class="col col-lg-3">         
            <input name="CriterioBusqueda" id="inputTextBusqueda" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Ingrese el apellido del paciente">                        
          </div>
          <div class="col col-lg-3">
            <button type="submit" class="btn btn-lg btn-success pt-1 pb-1 pr-3 pl-3 rounded">Buscar</button>
            <button type="button" class="btn btn-lg btn-success ml-3 pt-1 pb-1 pr-3 pl-3 rounded" onclick="window.history.back()">Volver</button>  
          </div>
        </div>  
      </form>
    </div>
    
    <div class="container shadow-lg bg-white pt-3 mt-4 pl-4 pr-4 pt-4 pb-4">
      <h4 class="font-weight-normal">Resultados de la búsqueda</h4>
        
      @if (isset($criterio))
        <div class="alert alert-primary alert-dismissible container mt-4" role="alert">
          <strong>Se buscó por criterio: {{$criterio}}  </strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      <table class="table table-hover  table-bordered mt-4">
        <thead class="thead-dark">
          <tr>
            <th scope="col" class="text-center">Apellido</th>
            <th scope="col" class="text-center">Nombre</th>
            <th scope="col" class="text-center">Estado</th>
             <th scope="col" class="text-center">Acciones</th>
          </tr>
        </thead>
        <tbody>     
          @if (count($pacientes) == 0)
            <td colspan="4" class="text-center align-middle font-weight-bold">No existen pacientes. </td>
          @endif
          @foreach ($pacientes as $paciente)
            <tr>
              <td class="text-center align-middle">{{ $paciente->apellido }}</td>
              <td class="text-center align-middle">{{ $paciente->nombre }}</td>
              <td class="text-center align-middle">{{ $paciente->estado }}</td>
              <td class="text-center align-middle">
              <a class="btn btn-success pl-3 pr-3 rounded" href="/pacientes/ver/{{ $paciente->id }}">Ver</a>
              <a class="btn btn-success pl-3 pr-3 rounded" href="/pacientes/editar/{{ $paciente->id }}">Modificar</a>               
            </tr>
          @endforeach
        </tbody>
        <div class="text-center mt-4">
           <!-- Al cambiar de página, se renderiza y no se pierden los filtros aplicados (estado) -->
            {!! $pacientes ->appends(Request::only(['Estado','type']))-> render() !!}           
        </div> 
        <!--          
        <ul class="list-group w-25 text-center">
          <li class="list-group-item justify-content-between align-items-center">
          Se encontraron {{count($pacientes)}} resultados
          </li>
        </ul>
        -->
      </table>
    </div>
  </div>

@endsection