<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class RestaurarController extends Controller
{
	public function restaurar() {
		$db_host = '127.0.0.1';
		$db_name = 'pacientesDB';
		$db_user = 'root';
		$db_pass = '';
		$archivo = basename($_FILES['inputFileRestauracion']['name']);	/* tomo el nombre del archivo subido */
		$rutaDestino = 'C:/Users/Martin/Desktop/'.'\\'.$archivo;		/* creo la ruta destino */
		move_uploaded_file($_FILES['inputFileRestauracion']['tmp_name'], $rutaDestino); /* copio de la carpeta temporal al escritorio */
		$comando = "C:/xampp/mysql/bin/mysql --user=".$db_user." --password=".$db_pass." --host=".$db_host." ".$db_name." < $rutaDestino";

		system($comando, $output);

		echo ("<script LANGUAGE='JavaScript'>
    			window.alert('Se restauró correctamente Pacientes en el sistema.');
    			window.location.href='http://pacientes/pacientes/buscarPorCriterioTodos';
    			</script>");
	}
}

?>