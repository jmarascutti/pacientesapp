<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PacientesController extends Controller
{

	public function crear(Request $request) {
		// return $request-->all();

		$request->validate([
			'nombre' => 'required',
			'apellido' => 'required',			
			'tel'  => 'required_without_all:tel,cel',
		], [
			 'nombre.required' => 'El nombre del paciente es obligatorio.',
			 'apellido.required' => 'El apellido del paciente es obligatorio.',
			 'tel.required_without_all' => 'El teléfono o el celular del paciente es obligatorio.'			 			 
		]);	

		$paciente = new App\Paciente;
		$paciente->apellido = $request->apellido;
		$paciente->nombre = $request->nombre;
		$paciente->edad = $request->edad;
		$paciente->dni = $request->dni;		
		$paciente->fec_nac = $request->fec_nac;
		$paciente->direccion = $request->direccion;
		$paciente->cel = $request->cel;
		$paciente->tel = $request->tel;
		$paciente->mail = $request->mail;
		$paciente->ob_soc = $request->ob_soc;
		$paciente->num_ob_soc = $request->num_ob_soc;
		$paciente->estado = $request->estado;
		$paciente->observaciones = $request->observaciones;

		$paciente->save();

		return back()->with('mensaje', 'PACIENTE DADO DE ALTA');

	}

	public function buscarPorCriterioTodos() {
		$pacientes = App\Paciente::orderBy('apellido', 'ASC')->Simplepaginate(10);

		return view('buscarPorCriterio')
		->with('pacientes', $pacientes);
	}

	public function buscarPorCriterio(Request $request) {
		$criterio = $request->get('Criterio');
		$criterioBusqueda = $request->get('CriterioBusqueda');

		if ($criterio == 'Buscar por apellido') {
			$pacientes = App\Paciente::where('apellido','like',"%$criterioBusqueda%")->orderBy('apellido', 'ASC')->Simplepaginate(10);
		} else {
			$pacientes = App\Paciente::where('nombre','like',"%$criterioBusqueda%")->orderBy('apellido', 'ASC')->Simplepaginate(10);
		}	

		return view('buscarPorCriterio')
		->with('pacientes', $pacientes)->with('criterio', $criterio);	
	}

	public function buscarPorEstadoTodos() {
		$pacientes = App\Paciente::orderBy('apellido', 'ASC')->Simplepaginate(10);

		return view('buscarPorEstado')
		->with('pacientes', $pacientes);
	}

	public function buscarPorEstado(Request $request) {
		$estado = $request->get('Estado');
		$pacientes = App\Paciente::where('estado','=',"$estado")->orderBy('apellido', 'ASC')->Simplepaginate(10);

		return view('buscarPorEstado')
		->with('pacientes', $pacientes)->with('estado', $estado); 
		
	}

	public function editar($id) {
		$paciente = App\Paciente::findOrFail($id);

		return view('editar', compact('paciente'));
	}

	public function update(Request $request, $id) {
		$pacienteUpdate = App\Paciente::findOrFail($id);

		$request->validate([
			'nombre' => 'required',
			'apellido' => 'required',			
			'tel'  => 'required_without_all:tel,cel',
		], [
			 'nombre.required' => 'El nombre del paciente es obligatorio.',
			 'apellido.required' => 'El apellido del paciente es obligatorio.',
			 'tel.required_without_all' => 'El teléfono o el celular del paciente es obligatorio.'			 			 
		]);	

		$pacienteUpdate->apellido = $request->apellido;
		$pacienteUpdate->nombre = $request->nombre;
		$pacienteUpdate->edad = $request->edad;
		$pacienteUpdate->dni = $request->dni;		
		$pacienteUpdate->fec_nac = $request->fec_nac;
		$pacienteUpdate->direccion = $request->direccion;
		$pacienteUpdate->cel = $request->cel;
		$pacienteUpdate->tel = $request->tel;
		$pacienteUpdate->mail = $request->mail;
		$pacienteUpdate->ob_soc = $request->ob_soc;
		$pacienteUpdate->num_ob_soc = $request->num_ob_soc;
		$pacienteUpdate->estado = $request->estado;
		$pacienteUpdate->observaciones = $request->observaciones;

		$pacienteUpdate->save();

		return back()->with('mensaje', 'PACIENTE ACTUALIZADO CORRECTAMENTE');
	}

	public function ver($id) {
		$paciente = App\Paciente::findOrFail($id);

		return view('ver', compact('paciente'));	
	}

	public function resguardarInformacion(Request $request) {
		
	}

	public function restaurarInformacion(Request $request) {
		
	}

}
