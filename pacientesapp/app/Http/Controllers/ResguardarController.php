<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ResguardarController extends Controller
{
	public function resguardar() {
		$db_host = '127.0.0.1';
		$db_name = 'pacientesDB';
		$db_user = 'root';
		$db_pass = '';

		$rutaDestino = 'C:/Users/Martin/Desktop/pacientesDB.sql';
		$dump = "C:/xampp/mysql/bin/mysqldump --no-defaults --user=".$db_user." --password=".$db_pass." --host=".$db_host." ".$db_name." > $rutaDestino";
		
		system($dump, $output);

		echo ("<script LANGUAGE='JavaScript'>
    			window.alert('El resguardo de pacientes ha sido generado correctamente en el escritorio de su equipo.');
    			window.location.href='http://pacientes/pacientes/buscarPorCriterioTodos';
    			</script>");				
	}
}

?>