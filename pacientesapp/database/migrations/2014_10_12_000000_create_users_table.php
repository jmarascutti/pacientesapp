<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('apellido');
            $table->string('nombre');
            $table->string('edad');
            $table->string('dni')->nullable();
            $table->string('fec_nac')->nullable();
            $table->string('direccion')->nullable();
            $table->string('cel')->nullable();
            $table->string('tel')->nullable();
            $table->string('mail')->nullable();
            $table->string('ob_soc')->nullable();
            $table->string('num_ob_soc')->nullable();
            $table->string('estado')->nullable();
            $table->string('observaciones')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
